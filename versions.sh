#!/usr/bin/env bash
set -Eeuo pipefail

needUpdateCiYml=

distsSuitesArches=( "$@" )
if [ "${#distsSuitesArches[@]}" -eq 0 ]; then
	distsSuitesArches=( */*/*/ )
	json='{}'
	needUpdateCiYml=yes
else
	json="$(< versions.json)"
fi
distsSuitesArches=( "${distsSuitesArches[@]%/}" )
allSuites=( "${distsSuitesArches[@]%/}" )
allSuites=( "${allSuites[@]%/*}" )
allSuites=( $(echo "${allSuites[@]#*/}" | tr ' ' '\n' | sort -u | tr '\n' ' ') )

declare -A knownIssues=(
	['debian/buster/armel']='https://gitlab.com/vicamo/docker-buildpack-deps/-/issues/60'
	['debian/buster/mips']='https://gitlab.com/vicamo/docker-buildpack-deps/-/issues/60'
	['debian/buster/mips64el']='https://gitlab.com/vicamo/docker-buildpack-deps/-/issues/60'
	['debian/buster/mipsel']='https://gitlab.com/vicamo/docker-buildpack-deps/-/issues/60'
	['debian/buster/ppc64el']='https://gitlab.com/vicamo/docker-buildpack-deps/-/issues/60'
	['debian/buster/s390x']='https://gitlab.com/vicamo/docker-buildpack-deps/-/issues/60'
	['debian/sid/alpha']='https://github.com/vicamo/docker_buildpack-deps/issues/36'
	['debian/sid/hppa']='https://github.com/vicamo/docker_buildpack-deps/issues/58'
	['debian/sid/hurd-i386']='https://github.com/vicamo/docker_buildpack-deps/issues/6'
	['debian/sid/kfreebsd-amd64']='https://github.com/vicamo/docker_buildpack-deps/issues/4'
	['debian/sid/kfreebsd-i386']='https://github.com/vicamo/docker_buildpack-deps/issues/5'
	['debian/sid/m68k']='https://github.com/vicamo/docker_buildpack-deps/issues/58'
	['debian/sid/powerpc']='https://github.com/vicamo/docker_buildpack-deps/issues/51'
	['debian/sid/powerpcspe']='https://github.com/vicamo/docker_buildpack-deps/issues/37'
	['debian/sid/ppc64']='https://github.com/vicamo/docker_buildpack-deps/issues/51'
	['debian/sid/riscv64']='https://gitlab.com/vicamo/docker-buildpack-deps/-/issues/61'
	['debian/sid/sh4']='https://github.com/vicamo/docker_buildpack-deps/issues/58'
	['debian/sid/sparc64']='https://github.com/vicamo/docker_buildpack-deps/issues/48'
	['debian/sid/x32']='https://github.com/vicamo/docker_buildpack-deps/issues/51'
	['ubuntu/focal/riscv64']='https://gitlab.com/vicamo/docker-buildpack-deps/-/issues/57'
	['ubuntu/xenial/s390x']='https://github.com/vicamo/docker-brew-ubuntu-debootstrap/issues/2'
)

qemuNativeArches=(amd64-i386 arm-armel armel-arm arm-armhf armhf-arm armel-armhf armhf-armel i386-amd64 powerpc-ppc64 ppc64-powerpc sparc-sparc64 sparc64-sparc s390-s390x s390x-s390)
qemuSuite_xenial=bionic

hostArch="$(dpkg --print-architecture)"

gitlabCiJobs=
for version in "${distsSuitesArches[@]}"; do
	arch="$(basename "$version")"
	codename="$(basename "$(dirname "$version")")"
	dist="$(dirname "$(dirname "$version")")"
	doc='{"variants": [ "curl", "scm", "" ]}'
	eval "suite=\${${codename}_suite:-}"
	if [ -z "$suite" ]; then
		case "$dist" in
		debian)
			# "stable", "oldstable", etc.
			eval "${codename}_suite=$(
				wget -qO- -o /dev/null "https://deb.debian.org/debian/dists/$codename/Release" \
					| gawk -F ':[[:space:]]+' '$1 == "Suite" { print $2 }' || true
			)"
			;;
		ubuntu)
			eval "${codename}_suite=$(
				wget -qO- -o /dev/null "http://archive.ubuntu.com/ubuntu/dists/$codename/Release" \
					| gawk -F ':[[:space:]]+' '$1 == "Version" { print $2 }' || true
			)"
			;;
		esac
		eval "suite=\${${codename}_suite:-}"
	fi
	if [ -n "$suite" ]; then
		export suite
		doc="$(jq <<<"$doc" -c '.suite = env.suite')"
		echo "$version: $suite"
	else
		echo "$version: ???"
	fi
	export doc version
	json="$(jq <<<"$json" -c --argjson doc "$doc" '.[env.version] = $doc')"

	gitlabCiJobs+="build:$dist:$codename:$arch:\n  extends: .build-template"
	if [[ ! " $hostArch-$hostArch ${qemuNativeArches[*]} " =~ $hostArch-$arch ]]; then
		qemuSuiteVar="qemuSuite_$codename"
		qemuSuite="${!qemuSuiteVar:-$codename}"
		if [ "$qemuSuite" != "$codename" ]; then
			gitlabCiJobs+="\n  variables:\n    JOB_QEMU_SUITE: '$qemuSuite'"
		fi
	fi
	case " ${!knownIssues[*]} " in
	*" ${version} "*)
		gitlabCiJobs+="\n  # ${knownIssues[${version}]}\n  allow_failure: true"
		;;
	esac
	gitlabCiJobs+="\n\n"
done

for codename in "${allSuites[@]}"; do
	gitlabCiJobs+="deploy:$codename:"
	gitlabCiJobs+="\n  extends: .deploy-template"
	gitlabCiJobs+="\n  rules:"
	gitlabCiJobs+="\n    - if: \$CI_COMMIT_BRANCH == \$CI_DEFAULT_BRANCH"
	gitlabCiJobs+="\n\n"
done

jq <<<"$json" -S . > versions.json

if [ -n "${needUpdateCiYml}" ]; then
  gitlabCi="$(awk -v 'RS=\n\n' '/^## BEGIN GENERATED JOBS ##/,/^## END GENERATED JOBS ##/ { if ($0 ~ /^## BEGIN/) $0 = "## BEGIN GENERATED JOBS ##\n\n'"${gitlabCiJobs}"'## END GENERATED JOBS ##"; else next; } { printf "%s%s", $0, RS }' .gitlab-ci.yml)"
  echo "$gitlabCi" > .gitlab-ci.yml
fi
